package pl.ims4p.wikitest.di

import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import pl.ims4p.wikitest.model.database.AppDatabase

val roomModule = module {

    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "wikia-db")
            .build()
    }

    single { get<AppDatabase>().expandedWikiaResultSetDao() }
    single { get<AppDatabase>().expandedWikiaItemDao() }
    single { get<AppDatabase>().imageDimensionsDao() }
    single { get<AppDatabase>().wikiaStatsDao() }

}
