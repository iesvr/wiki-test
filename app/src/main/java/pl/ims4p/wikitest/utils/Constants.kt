package pl.ims4p.wikitest.utils

/** The base URL of the API */
const val BASE_URL: String = "https://community.fandom.com"