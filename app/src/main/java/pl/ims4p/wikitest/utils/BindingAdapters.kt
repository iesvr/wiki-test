package pl.ims4p.wikitest.utils

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value?:View.VISIBLE})
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value?:""})
    }
}

@BindingAdapter("glideImage")
fun setGlideUrl(imageView: ImageView, glideImage: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = imageView.getParentActivity()
    if(parentActivity != null && glideImage != null) {
        glideImage.observe(parentActivity, Observer { value ->
            Glide.with(imageView.context)
                .apply {
                    RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                }
                .load(value)
                .into(imageView)
        })
    }
}
