package pl.ims4p.wikitest.utils

fun countToString(value: Long): String {
    val suffixes = arrayOf("", "k", "M", "G")
    var suffixIndex = 0

    var quantity = value

    while (quantity >= 1000L) {
        quantity = quantity / 1000L
        suffixIndex++
    }

    return "$quantity${suffixes[suffixIndex]}"
}