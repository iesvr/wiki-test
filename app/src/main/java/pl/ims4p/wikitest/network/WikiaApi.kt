package pl.ims4p.wikitest.network

import pl.ims4p.wikitest.model.ExpandedWikiaResultSet
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import java.util.*

interface WikiaApi {

    @GET("/api/v1/Wikis/List?expand=1&limit=30")
    @Headers("Accept: application/json")
    fun getPages(): Observable<ExpandedWikiaResultSet>

}