package pl.ims4p.wikitest.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(
    tableName = "image_dimensions",
    foreignKeys = [
        ForeignKey(
            entity = ExpandedWikiaItem::class,
            parentColumns = ["id"],
            childColumns = ["ownerId"],
            onDelete = CASCADE)
    ]
)
data class ImageDimensions(

    @field:PrimaryKey(autoGenerate = true)
    var id: Long,
    var ownerId: Long,

    val width: Int,
    val height: Int
)
