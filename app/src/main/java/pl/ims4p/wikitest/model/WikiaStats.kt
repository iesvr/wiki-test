package pl.ims4p.wikitest.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(
    tableName = "wikia_stats",
    foreignKeys = [
        ForeignKey(
            entity = ExpandedWikiaItem::class,
            parentColumns = ["id"],
            childColumns = ["ownerId"],
            onDelete = CASCADE)
    ]
)
data class WikiaStats(

    @field:PrimaryKey(autoGenerate = true)
    var id: Long,
    var ownerId: Long,

    val users: Long,
    val articles: Long,
    val pages: Long,
    val admins: Long,
    val activeUsers: Long,
    val edits: Long,
    val videos: Long,
    val images: Long,
    val discussions: Long

)