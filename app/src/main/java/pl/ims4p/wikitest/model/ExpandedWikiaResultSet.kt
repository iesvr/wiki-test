package pl.ims4p.wikitest.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wikia_result_sets")
data class ExpandedWikiaResultSet(

    @field:PrimaryKey(autoGenerate = true)
    var id: Long,

    val batches: Int,
    val total: Int,
    val currentBatch: Int,
    val next: Int,

    val items: List<ExpandedWikiaItem>
)
