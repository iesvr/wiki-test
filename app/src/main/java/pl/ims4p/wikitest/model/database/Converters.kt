package pl.ims4p.wikitest.model.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pl.ims4p.wikitest.model.ExpandedWikiaItem
import pl.ims4p.wikitest.model.ImageDimensions
import pl.ims4p.wikitest.model.WikiaStats
import java.lang.reflect.Type

class Converters {

    inline fun <reified T> genericType() = object: TypeToken<T>() {}.type

    @TypeConverter
    fun wikiaItemsToJson(items: List<ExpandedWikiaItem>): String = Gson().toJson(items)

    @TypeConverter
    fun jsonToWikiaItems(json: String): List<ExpandedWikiaItem> = Gson().fromJson(json, genericType<List<ExpandedWikiaItem>>())

    @TypeConverter
    fun imageDimensionsToJson(imageDimensions: ImageDimensions): String = Gson().toJson(imageDimensions)

    @TypeConverter
    fun jsonToImageDimensions(json: String): ImageDimensions = Gson().fromJson(json, ImageDimensions::class.java)

    @TypeConverter
    fun wikiaStatsToJson(wikiaStats: WikiaStats): String = Gson().toJson(wikiaStats)

    @TypeConverter
    fun jsonToWikiaStats(json: String): WikiaStats = Gson().fromJson(json, WikiaStats::class.java)

    @TypeConverter
    fun stringListToJson(items: List<String>): String = Gson().toJson(items)

    @TypeConverter
    fun jsonToStringList(json: String): List<String> = Gson().fromJson(json, genericType<List<String>>())

}