package pl.ims4p.wikitest.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.ims4p.wikitest.model.WikiaStats

@Dao
interface WikiaStatsDao {

    @get:Query("SELECT * FROM wikia_stats")
    val all: List<WikiaStats>

    @Query("SELECT * FROM wikia_stats WHERE ownerId = :wikiaItemId")
    fun getAllByWikiaItemId(wikiaItemId: Long): List<WikiaStats>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg wikiaStats: WikiaStats)

}
