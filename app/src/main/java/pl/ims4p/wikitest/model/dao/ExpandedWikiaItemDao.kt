package pl.ims4p.wikitest.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.ims4p.wikitest.model.ExpandedWikiaItem

@Dao
interface ExpandedWikiaItemDao {

    @get:Query("SELECT * FROM wikia_items")
    val all: List<ExpandedWikiaItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg expandedWikiaItem: ExpandedWikiaItem)

}
