package pl.ims4p.wikitest.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.ims4p.wikitest.model.ExpandedWikiaItem
import pl.ims4p.wikitest.model.ExpandedWikiaResultSet
import pl.ims4p.wikitest.model.ImageDimensions
import pl.ims4p.wikitest.model.WikiaStats
import pl.ims4p.wikitest.model.dao.ExpandedWikiaItemDao
import pl.ims4p.wikitest.model.dao.ExpandedWikiaResultSetDao
import pl.ims4p.wikitest.model.dao.ImageDimensionsDao
import pl.ims4p.wikitest.model.dao.WikiaStatsDao

@Database(entities = arrayOf(ExpandedWikiaResultSet::class, ExpandedWikiaItem::class, ImageDimensions::class, WikiaStats::class),
    version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun expandedWikiaResultSetDao(): ExpandedWikiaResultSetDao
    abstract fun expandedWikiaItemDao(): ExpandedWikiaItemDao
    abstract fun imageDimensionsDao(): ImageDimensionsDao
    abstract fun wikiaStatsDao(): WikiaStatsDao

}