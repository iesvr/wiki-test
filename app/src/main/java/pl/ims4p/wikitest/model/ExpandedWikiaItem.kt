package pl.ims4p.wikitest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "wikia_items")
data class ExpandedWikiaItem(

    @field:PrimaryKey
    var id: Long,

    val name: String,
    val hub: String,
    val language: String,
    val topic: String,
    val domain: String,
    val woodmark: String?,
    val title: String,
    val url: String,

    val stats: WikiaStats,
    val topUsers: List<String>, //lista Int

    @ColumnInfo(name = "founding_user_id")
    val foundingUserId: String, //powinno byc Int

    @ColumnInfo(name = "creation_date")
    val creationDate: String,

    val headline: String,
    val lang: String,
    val flags: List<String>,
    val desc: String,
    val image: String,

    @ColumnInfo(name = "wam_score")
    val wamScore: String,

    val originalDimensions: ImageDimensions

)