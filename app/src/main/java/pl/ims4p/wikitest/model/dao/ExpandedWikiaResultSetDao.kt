package pl.ims4p.wikitest.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.ims4p.wikitest.model.ExpandedWikiaResultSet

@Dao
interface ExpandedWikiaResultSetDao {

    @get:Query("SELECT * FROM wikia_result_sets")
    val all: List<ExpandedWikiaResultSet>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg expandedWikiaResultSet: ExpandedWikiaResultSet)

}