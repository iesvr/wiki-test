package pl.ims4p.wikitest.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.ims4p.wikitest.model.ImageDimensions

@Dao
interface ImageDimensionsDao {

    @get:Query("SELECT * FROM image_dimensions")
    val all: List<ImageDimensions>

    @Query("SELECT * FROM image_dimensions WHERE ownerId = :wikiaItemId")
    fun getAllByWikiaItemId(wikiaItemId: Long): List<ImageDimensions>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg imageDimensions: ImageDimensions)

}
