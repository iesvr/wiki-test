package pl.ims4p.wikitest

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import pl.ims4p.wikitest.databinding.ActivityTopWikisBinding
import pl.ims4p.wikitest.ui.main.SectionsPagerAdapter

class TopWikisActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTopWikisBinding
    private lateinit var viewModel: TopWikisViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_top_wikis)
        viewModel = ViewModelProviders.of(this).get(TopWikisViewModel::class.java)
        binding.viewModel = viewModel

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        binding.viewPager.adapter = sectionsPagerAdapter

        binding.tabs.setupWithViewPager(binding.viewPager)
    }
}
