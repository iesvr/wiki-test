package pl.ims4p.wikitest.ui.images

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.databinding.FragmentImageListBinding

class ImageListFragment : Fragment() {

    private lateinit var viewModel: ImageListViewModel
    private lateinit var binding: FragmentImageListBinding

    val imagesAdapter: ImagesAdapter = ImagesAdapter()

    var errorSnackbar: Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate<FragmentImageListBinding>(inflater, R.layout.fragment_image_list, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ImageListViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })

        binding.viewModel = viewModel

        viewModel.getPages().observe(this, Observer { data ->
            data.let {
                imagesAdapter.apply {
                    updateItems(ArrayList(it))
                }
            }
        })

        viewModel.loadPages()

        binding.wikiGrid.apply {
            adapter = imagesAdapter
        }
    }

    fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.common_retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    companion object {
        fun newInstance() = ImageListFragment()
    }

}
