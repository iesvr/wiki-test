package pl.ims4p.wikitest.ui.base

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.di.createApi
import pl.ims4p.wikitest.model.ExpandedWikiaItem
import pl.ims4p.wikitest.model.dao.ExpandedWikiaResultSetDao
import pl.ims4p.wikitest.network.WikiaApi
import retrofit2.Retrofit
import timber.log.Timber

open class ListViewModel : ViewModel(), KoinComponent {

    val retrofit: Retrofit by inject()
    val wikiaApi: WikiaApi = createApi<WikiaApi>(retrofit)
    val expandedWikiaResultSetDao: ExpandedWikiaResultSetDao by inject()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPages() }

    val pages: MutableLiveData<List<ExpandedWikiaItem>> = MutableLiveData()

    var disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun getPages(): LiveData<List<ExpandedWikiaItem>> = pages as LiveData<List<ExpandedWikiaItem>>

    fun loadPages() {
        disposable.add(
            wikiaApi.getPages()
                .concatMap { resultSet ->
                    expandedWikiaResultSetDao.insertAll(resultSet)
                    Observable.just(expandedWikiaResultSetDao.all.first())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePagesListStart() }
                .doOnTerminate { onRetrievePagesListFinish() }

                .subscribe(
                    { result -> onRetrievePagesListSuccess(result.items) },
                    { error -> onRetrievePagesListError(error) }
                )
        )
    }

    private fun onRetrievePagesListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePagesListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePagesListSuccess(pagesList: List<ExpandedWikiaItem>) {
        loadingVisibility.value = View.GONE
        pages.value = pagesList
    }

    private fun onRetrievePagesListError(error: Throwable) {
        loadingVisibility.value = View.GONE
        Timber.e("EXCEPTION: ${error.message}")

        disposable.add(
            Observable.fromCallable { expandedWikiaResultSetDao.all.first() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    onRetrievePagesListSuccess(it.items)
                }
        )

        errorMessage.value = R.string.common_post_error
    }

}