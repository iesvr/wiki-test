package pl.ims4p.wikitest.ui.images

import android.content.Context
import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.ims4p.wikitest.model.ExpandedWikiaItem

class ImageItemViewModel: KoinComponent {

    val context: Context by inject()

    private val image = MutableLiveData<String>()

    fun bind(item: ExpandedWikiaItem) {
        image.value = item.image
    }

    fun getImage(): MutableLiveData<String> = image

}
