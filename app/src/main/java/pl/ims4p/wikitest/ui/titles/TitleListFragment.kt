package pl.ims4p.wikitest.ui.titles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.databinding.FragmentTitleListBinding

class TitleListFragment : Fragment() {

    private lateinit var viewModel: TitleListViewModel
    private lateinit var binding: FragmentTitleListBinding

    val titlesAdapter: TitlesAdapter = TitlesAdapter()

    var errorSnackbar: Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate<FragmentTitleListBinding>(inflater, R.layout.fragment_title_list, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TitleListViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })

        binding.viewModel = viewModel

        viewModel.getPages().observe(this, Observer { data ->
            data.let {
                titlesAdapter.apply {
                    updateItems(ArrayList(it))
                }
            }
        })

        viewModel.loadPages()

        binding.wikiList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = titlesAdapter
        }
    }

    fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.common_retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    companion object {
        fun newInstance() = TitleListFragment()
    }

}
