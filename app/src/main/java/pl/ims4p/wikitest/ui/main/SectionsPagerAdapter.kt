package pl.ims4p.wikitest.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.ui.images.ImageListFragment
import pl.ims4p.wikitest.ui.titles.TitleListFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_titles_text,
    R.string.tab_images_text
)

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return TitleListFragment.newInstance()
        } else {
            return ImageListFragment.newInstance()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }
}