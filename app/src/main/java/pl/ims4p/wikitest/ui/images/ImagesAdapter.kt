package pl.ims4p.wikitest.ui.images

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.databinding.ViewImageItemBinding
import pl.ims4p.wikitest.model.ExpandedWikiaItem

class ImagesAdapter(): BaseAdapter() {

    private lateinit var items: List<ExpandedWikiaItem>

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ViewHolder

        if (convertView == null) {
            val binding: ViewImageItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent!!.context), R.layout.view_image_item, parent, false)
            viewHolder = ViewHolder(binding, binding.root)

            view = binding.root
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.bind(items[position])

        return view as View
    }

    override fun getItem(position: Int): ExpandedWikiaItem = items[position]

    override fun getItemId(position: Int): Long = items[position].id

    override fun getCount(): Int =
        if (::items.isInitialized) items.size else 0

    fun updateItems(items: List<ExpandedWikiaItem>){
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ViewImageItemBinding, row: View?) {
        private val viewModel = ImageItemViewModel()

        fun bind(item: ExpandedWikiaItem) {
            viewModel.bind(item)
            binding.viewModel = viewModel
        }
    }

}
