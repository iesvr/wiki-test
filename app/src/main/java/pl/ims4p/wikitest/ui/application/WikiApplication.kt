package pl.ims4p.wikitest.ui.application

import android.app.Application
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.ims4p.wikitest.BuildConfig
import pl.ims4p.wikitest.di.appModule
import pl.ims4p.wikitest.di.networkModule
import pl.ims4p.wikitest.di.roomModule
import timber.log.Timber

class WikiApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@WikiApplication)

            modules(listOf(
                appModule,
                networkModule,
                roomModule
            ))
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(Timber.asTree())
        }

        Stetho.initializeWithDefaults(this)
    }

}