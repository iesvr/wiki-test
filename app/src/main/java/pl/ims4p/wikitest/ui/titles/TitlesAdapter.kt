package pl.ims4p.wikitest.ui.titles

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pl.ims4p.wikitest.R
import pl.ims4p.wikitest.databinding.ViewTitleItemBinding
import pl.ims4p.wikitest.model.ExpandedWikiaItem

class TitlesAdapter: RecyclerView.Adapter<TitlesAdapter.ViewHolder>() {

    private lateinit var items: List<ExpandedWikiaItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitlesAdapter.ViewHolder {
        val binding: ViewTitleItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.view_title_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TitlesAdapter.ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int =
        if (::items.isInitialized) items.size else 0

    fun updateItems(items: List<ExpandedWikiaItem>){
        this.items = items
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ViewTitleItemBinding): RecyclerView.ViewHolder(binding.root) {
        private val viewModel = TitleItemViewModel()

        fun bind(item: ExpandedWikiaItem) {
            viewModel.bind(item)
            binding.viewModel = viewModel
        }
    }

}
