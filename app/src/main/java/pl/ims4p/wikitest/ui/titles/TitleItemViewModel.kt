package pl.ims4p.wikitest.ui.titles

import android.content.Context
import androidx.lifecycle.MutableLiveData
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.ims4p.wikitest.model.ExpandedWikiaItem
import pl.ims4p.wikitest.utils.countToString

class TitleItemViewModel: KoinComponent {

    val context: Context by inject()

    private val title = MutableLiveData<String>()
    private val articles = MutableLiveData<String>()

    fun bind(item: ExpandedWikiaItem) {
        title.value = item.name
        articles.value = countToString(item.stats.articles)
    }

    fun getTitle(): MutableLiveData<String> = title

    fun getArticles(): MutableLiveData<String> = articles

}
